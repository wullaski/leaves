leaves
======

**Getting Started**

Navigate to the public directory and install dependencies with npm.

``` 
$ npm install
```

Install Grunt Globally

```
$ npm install -g grunt-cli
```

Run grunt watch for development. See Gruntfile.js for other commands.

```
$ grunt watch
```


**Todo**

- [ ] Add a move function that moves a player to another room
- [ ] Add multiplayer with firebase
- [ ] Add more items
- [ ] Add lock and key functionality
