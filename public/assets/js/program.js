var leaves = (function (app) {

    //Create Items
    //TODO: Fix this maybe -> The order the items are created are important
    //items first then containers followed by room and player
    var items = [];

    items.push(new app.Item({
      id : 0,
      descriptor : ["flint"],
      combineWith : ["steel"],
      physicalSize : 1
    }));
    items.push(new app.Item({
      id : 1,
      descriptor : ["stick"],
      physicalSize : 24
    }));
    items.push(new app.Item({
      id : 14,
      descriptor : ["string"],
      physicalSize : 1
    }));
    items.push(new app.Item({
      id : 2,
      descriptor : ["rune"],
      sightDescription : "It is a stone with a rune carved in it. It probably has hidden power.",
      physicalSize: 1,
      combineWith: ["keensword"]
    }));
    items.push(new app.Item({
      id : 3,
      descriptor : ["stone", "flagstone", "rock"],
      physicalSize : 1,
      combineWith : ["sword"],
      saveOnCombine : true,
    }));
    items.push(new app.Item({
      id : 4,
      descriptor : ["steel"],
      physicalSize : 1
    }));
    items.push(new app.Item({
      id : 5,
      descriptor : ["tinderbox"],
      sightDescription : "you can light stuff with it",
      comprisedOf : ["Flint", "Steel"],
      physicalSize : 2
    }));
    items.push(new app.Item({
      id : 6,
      descriptor : ["pouch"],
      containedItems : [2],
      physicalSize : 4,
      capacity : 4
    }));
    items.push(new app.Item({
      id : 7,
      descriptor : ["bag2"],
      containedItems : [],
      physicalSize : 17,
      capacity : 16
    }));
    items.push(new app.Item({
      id : 8,
      descriptor : ["bag"],
      containedItems : [4,5,6],
      physicalSize : 17,
      capacity : 16
    }));
    items.push(new app.Item({
      id : 9,
      isStationary : true,
      descriptor : ["puddle"],
      physicalSize : 64,
      capacity : 64,
      containedItems : [0,1,3,7],
      visualSecretThreshold : 6,
      sightDescription : "Rings of light ripple out from the center as drops fall into it from above.",
      visualSecret : "As you look closer you can see that there is some depth to it!",
      sounds : "The only sounds are those of the liquid dripping into it.",
      tastes : "It tastes like burning.",
      smells : "The noxious smelling liquid leaves you feeling light headed."
    }));
    items.push(new app.Item({
      id : 10,
      descriptor : ["capris"],
      sightDescription : "Too short of pants too long for shorts.",
      sounds : "They make a quiet swishing sound when you walk (stealth -1).",
      tastes : "You probably don't want to do that.",
      smells : "You probably don't want to do that.",
      touch : "They feel light and you feel agile, like a cat. (agility +2).",
      physicalSize : 3,
      containedItems : [14],
      capacity : 2,
      combineWith: ["keensword"],
    }));
    items.push(new app.Item({
      id : 11,
      descriptor : ["sword"],
      getting : "You pick up the sword.",
      sightDescription : "The blade is pitted with age.",
      touch: "You carefully rub your thumb across different points on the blade. It would benefit from a good sharpening, but it does look like quality steel.",
      physicalSize: 24,
      combineWith: ["stone"]
    }));
    items.push(new app.Item({
      id : 12,
      descriptor : ["Keen Sword"],
      getting: "This is finely honed sword. Definitely able to make quick work of those awful capris and convert them into proper shorts.",
      touch: "It feels as though you could shave with it. Not that you should try.",
      sightDescription: "It's a standard short sword, double bladed and made for slashing.",
      physicalSize: 24,
      comprisedOf: ["sword", "stone"],
      combineWith: ["capris", "rune"],
      saveOnCombine: true
    }));
    items.push(new app.Item({
      id : 13,
      descriptor: ['shorts'],
      getting: "You've aquired leg coverings of appropriate length.",
      sightDescription: "Shorts with pockets big enough to carry at least a few dead birds or whatever it is you like to carry around with you.",
      physicalSize: 12,
      capacity: 24,
      containedItems: [],
      comprisedOf: ["Keen Sword", "Capris"],
    }));
    //Create Players array
    var players = [];
    players.push(new app.Player({
      playerId : 0,
      playerName : "Ambrose",
      containedItems : [],
      location: 0,
    }));
    players.push(new app.Player({
      playerId : 1,
      playerName : "Everett",
      location: 1,
    }));
    var currentPlayer = players[0];
    //Create Room Object passing descriptions and items in
    var rooms = [];
    rooms.push(new app.Room({
      roomId : 0,
      descriptor : ["room","cell","area","here"],
      ambientLight : 10,
      containedItems : [9],
      visualSecretThreshold : 5,
      visualSecret : "There is some writing scratched into the wall. Faint but clear. \"Don't climb you'll never make it.\"",
      sightDescription : "You are in a small room with roughly hewn stone walls joined together without mortar. No doors, no windows, just cold stone walls. The floor is of the same stone but larger and smoother tiles. If there is a roof you cannot tell as there is only darkness above you.",
      sounds : "drip... drip... drip... The dripping noise is slow and even. It sounds as though droplets are falling into a small puddle nearby.",
      touch : "It's cool where you are. You feel solid and cold stone beneath your feet.",
      smells : "You smell something that reminds you of lamp oil.",
      exits: [
        {
          name: 'Chain',
          longDescription: 'You climb hand over hand over cold steel links. Losing track of the distance as your muscles begin to burn and go numb from the excertion. You expect you\'ll get to the top and confront a solid stone cieling but you don\'t. You feel a draft and there\'s an opening. You climb through and like a still birth reality shifts away and you are back at the terminal.',
          shortDescriptions: 'You climb up the chain and through the opening. Again.',
          toLocation: 1,
        },
      ]
    }));
    rooms.push(new app.Room({
      roomId : 1,
      descriptor : ["room","inspection house","area","here"],
      ambientLight : 10,
      containedItems : [11],
      visualSecretThreshold : 5,
      visualSecret : "No secrets to be seen.",
      sightDescription : "You are in a small room.",
      sounds : "A ticking sound can be heard.",
      touch : "It feels warm and dry here.",
      smells : "It smells fine.",
      exits: [
        {
          name: 'Chain',
          longDescription: 'You climb down through the opening and carefully down the chain.',
          shortDescriptions: 'You climb down the chain again.',
          toLocation: 0,
        },
      ],
    }));

  //Testing function
  (function(){

    //declare some variables for the ui
    var textNode = document.getElementById("user-input"),
        inputNode = document.getElementById("text-input");

    inputNode.focus();

    document.getElementById('gateway').addEventListener('click', function(){
      inputNode.focus();
    })

    //read function
    var read = function(userCmd){
      var dict = {
        'help' : '<p>Use your senses for hints. (<span class="verb_hint">look, listen, feel, smell, taste)</span> There is also <span class="verb_hint">search, take, put, and combine</span>.<br />Example Commands:<br /><span class="verb_hint">look</span> <span class="noun_hint">puddle</span> <br /><span class="verb_hint">listen</span><br /><em>Enter commands below.</em></p>',
        'save' : '<p>Your progress would have been saved in the imperial scrolls of honor, but the developer is too busy playing videogames that work for that feature to implemented. Consider it hardcore mode for now.</p>',
      };
      var str = userCmd.toLowerCase(),
          words = str.split(" ");
      if (words[0] == "save") {
        textNode.parentNode.insertBefore(app.fn.htmlToElement(dict.save), textNode);
      }else if (words[0] == "help"){
        textNode.parentNode.insertBefore(app.fn.htmlToElement(dict.help), textNode);
      }else if (words[0] == "inv" || words[0] == "inventory"){
        narration = currentPlayer.checkInventory(currentPlayer, items);
        textNode.parentNode.insertBefore(app.fn.htmlToElement('<p>' + narration + '</p>'), textNode);
      }else if (words.length >= 1){
        var currentRoom = rooms.filter(room => room.roomId === currentPlayer.location);
        //have the player process the complete command
        narration = comprehend(words, currentRoom);
        textNode.parentNode.insertBefore(app.fn.htmlToElement('<p class="user-cmd">' + userCmd + '</p>'), textNode);
        textNode.parentNode.insertBefore(app.fn.htmlToElement('<p>' + narration + '</p>'), textNode);
      }
    };
    //comprehend function
    var comprehend = function(words, room){
      /*
      // This function takes the strArray from the user input and processes it into a usable command for the
      // currentPlayer functions. It then calls the function and passes item and room information.
      */
      // Set the verb and modify the words array

      var verb = '';
      var nouns = [];
      var allWords = words.length;
      for (var i = 0; i < allWords; i++){
        var word = words[i];
        if (typeof currentPlayer[word] === "function"){
          verb = words[i];
          var verbIndex = words.lastIndexOf(words[i]);
          words.splice(verbIndex, 1);
          break;
        }
      }
      //// Set a couple very useful variables here
      var playerItems = app.fn.getNestedItems(currentPlayer.containedItems);
      var roomItems = app.fn.getNestedItems(room.containedItems);
      var numWords = words.length; // after the verb is removed
          //console.log(roomItems)
          //console.log(playerItems)
      // push to the nouns array
      var availableItems = playerItems.concat(roomItems).concat(room),
          numItems = availableItems.length;
      if (numWords > 0){
        //loop the words and check it against the available items
        for (var j = 0; j < numWords; j++) {
          for (var k = 0; k < numItems; k++) {
            //Check the descriptor array against the words
            var numNames = availableItems[k].descriptor.length;
            for (var l = 0; l < numNames; l++) {
              // Check to see if any of the words match an available item and push the available item
              if (availableItems[k].descriptor[l].toLowerCase() === words[j].toLowerCase()) {
                nouns.push(availableItems[k]);
              }
            }
            // Check the exits array against the words
            // First check if the item has exits
            if (availableItems[k].exits){
              var numExits = availableItems[k].exits.length;
              for (var m = 0; m < numExits; m++) {
                if (availableItems[k].exits[m].name.toLowerCase() === words[j].toLowerCase()) {
                  nouns.push(words[j]);
                }
              }
            }
          }
        }
      }else{
        nouns.push(room);
      }
      //// call the function with the verb and nouns
      if (verb){
        var numNouns = nouns.length,
            itemLibrary = items;
        if (numNouns > 0){
          return currentPlayer[verb](nouns, room, itemLibrary);
        }else{
          return "There is no " + words.join(" ") + " for which to " + verb;
        }
      }else{
        return "Although it may be your wish to " + words.join(" ") + ". What would be the point?";
      }
    };
    //Place the cursor in the input
    inputNode.focus();
    //Run a function when user hits enter
    inputNode.addEventListener("keyup",function(event){
      if(event.keyCode === 13){
        var userCmd = event.target.value;
        if (userCmd !== ''){
          //run the read funtion
          read(userCmd);
          //clear the input
          event.target.value = '';
        }else{
          textNode.before("Sometimes the best course of action is to take no action, but that's not the case here.");
        }
        document.getElementById('readout').style.top = document.getElementById('readout-content').height;
      }
    });
  })();

  return app;
}(leaves || {}));